package com.mhmdjalal.basicstatecodelab

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

/**
 * @author Created by Muhamad Jalaludin on 14/10/2022
 */

@Composable
fun WellnessTaskList(
    modifier: Modifier = Modifier,
    onCheckedTask : (WellnessTask, Boolean) -> Unit,
    onCloseTask : (WellnessTask) -> Unit,
    list: List<WellnessTask>
) {
    LazyColumn(
        modifier = modifier,
        state = rememberLazyListState()
    ) {
        items(list) { task ->
            WellnessTaskItem(
                taskName = task.label,
                checked = task.checked,
                onCheckedChange = { checked -> onCheckedTask(task, checked) },
                onClose = { onCloseTask(task) }
            )
        }
    }
}